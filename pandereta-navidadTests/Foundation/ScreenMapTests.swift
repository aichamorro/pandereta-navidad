//
//  PanderetaScreenMapTests.swift
//  pandereta-navidadTests
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Quick
import Nimble
@testable import pandereta_navidad

class PanderetaScreenMapTests: QuickSpec {
    override func spec() {
        describe("Building") {
            it("Can associate a region with an event") {
                let event = InstrumentEvent(name: "Treble", userInfo: [:])
                let region = ScreenMapRegion(x: 0, y: 0, width: 0.15, height: 0.15)!
                let screenMap = InstrumentDeviceMap.Builder().add(region: region, event: event).build()

                expect(screenMap.mappings.count).to(equal(1))
            }

            it("Can add several regions") {
                let regionOne = ScreenMapRegion(x: 0, y: 0, width: 0.15, height: 0.15)!
                let regionTwo = ScreenMapRegion(x: 0, y: 0.85, width: 0.15, height: 0.15)!
                let event = InstrumentEvent(name: "Dummy", userInfo: [:])

                let screenMap = InstrumentDeviceMap.Builder()
                    .add(region: regionOne, event: event)
                    .add(region: regionTwo, event: event)
                    .build()

                expect(screenMap.mappings.count).to(equal(2))
            }
        }

        describe("Mapping") {
            describe("Given there is no associated mapping to the region") {
                var screenMap: InstrumentDeviceMap!

                beforeEach {
                    screenMap = InstrumentDeviceMap.Builder().build()
                }

                afterEach {
                    screenMap = nil
                }

                it("returns a (null) event") {
                    expect(screenMap.find(eventFor: ScreenMapPoint(0.5, 0.5))).to(beNil())
                }
            }

            describe("Given there is an associated mapping to a region") {
                it("returns the associated event") {
                    let event = InstrumentEvent(name: "Dummy", userInfo: [:])
                    let region = ScreenMapRegion(x: 0, y: 0, width: 0.15, height: 0.15)!
                    let screenMap = InstrumentDeviceMap.Builder().add(region: region, event: event).build()

                    let point = ScreenMapPoint(x: 0.05, y: 0.05)
                    let found = screenMap.find(eventFor: point)

                    expect(found).toNot(beNil())
                    expect(InstrumentEventAreEquals(event, found!)).to(beTrue())
                }

                it("returns nil if there is no associated event") {
                    let event = InstrumentEvent(name: "Dummy", userInfo: [:])
                    let region = ScreenMapRegion(x: 0, y: 0, width: 0.15, height: 0.15)!
                    let screenMap = InstrumentDeviceMap.Builder().add(region: region, event: event).build()

                    let point = ScreenMapPoint(x: 0.5, y: 0.05)
                    let found = screenMap.find(eventFor: point)

                    expect(found).to(beNil())
                }
            }
        }
    }
}
