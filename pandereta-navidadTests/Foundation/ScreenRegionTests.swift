//
//  ScreenRegionTests.swift
//  pandereta-navidadTests
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import pandereta_navidad

class ScreenRegionTests: QuickSpec {
    override func spec() {
        describe("Can create regions with coordinate 0") {
            it("Can create regions with coordinate-x=0") {
                expect(ScreenMapRegion(x: 0, y: 0.5, width: 1, height: 1)).toNot(beNil())
            }

            it("Can create regions with coordinate-y=0") {
                expect(ScreenMapRegion(x: 0.5, y: 0, width: 1, height: 1)).toNot(beNil())
            }

            it("Can create regions with both coordinates set to 0") {
                expect(ScreenMapRegion(x: 0, y: 0, width: 1, height: 1)).toNot(beNil())
            }
        }
    }
}
