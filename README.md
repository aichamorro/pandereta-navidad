# Introducción
Pandereta navidad es una aplicación para iOS con la cual se pueden tocar varios instrumentos típicos navideños: Pandereta, Zambomba, botella de anís, etc. Además, contiene patrones para poder estudiar las canciones navideñas y llegar a disfrutar las Navidades como todo un profesional de la percusión navideña.

# Instalación

# Desarrollo
Pandereta navidad require una instalación válida de Xcode y CocoaPods.

# Autores
 * Alberto Chamorro
 * Rocío Ortiz
