//
//  AppDelegate.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import UIKit
import AudioKit

private let TimelineHeight: CGFloat = 200

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mInstrumentDeviceMap: InstrumentDeviceMap!
    var soundPlayer: SoundPlayer!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()

        soundPlayer = AKSoundPlayer()
        soundPlayer.instrument = TambourineInstrument()
        soundPlayer.start()

        mInstrumentDeviceMap = InstrumentDeviceMapTambourine()
        let instrumentViewController = InstrumentViewController()
        instrumentViewController.delegate = self

        window?.rootViewController = instrumentViewController

        return true
    }
}

extension AppDelegate: InstrumentViewControllerDelegate {
    func shouldDrawRegions() -> Bool {
        return false
    }

    func instrumentDeviceMap() -> InstrumentDeviceMap? {
        return mInstrumentDeviceMap
    }

    func instrumentViewController(_ viewController: InstrumentViewController, instrumentEvent: InstrumentEvent, intensity: Float) {
        print("\(instrumentEvent.name), intensity: \(intensity)")
        soundPlayer.instrument?.play(event: instrumentEvent, intensity: intensity)
    }

    func backgroundColor(for event: InstrumentEvent) -> UIColor {
        return ColorMapTambourine(forEvent: event)
    }
}
