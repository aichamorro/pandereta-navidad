//
//  ViewFromScreenMap.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 05/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit

func ViewFromCircleScreenMap(_ region: CircleScreenMapRegion,
                             screenMapPointToCGPoint: (ScreenMapPoint) -> CGPoint,
                             color: UIColor?) -> UIView {
    let origin = screenMapPointToCGPoint(region.origin)
    let size = screenMapPointToCGPoint(region.size)

    let frame = CGRect(x: origin.x - size.x / 2,
                       y: origin.y - size.x / 2,
                       width: size.x,
                       height: size.x)

    let view = UIView(frame: frame)
    let circlePath  = UIBezierPath(arcCenter: CGPoint(x: frame.width/2, y: frame.height/2),
                                   radius: screenMapPointToCGPoint((region.radius, region.radius)).x,
                                   startAngle: 0,
                                   endAngle: CGFloat(Double.pi * 2),
                                   clockwise: true)
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = circlePath.cgPath
    shapeLayer.fillColor = color?.cgColor ?? UIColor.black.cgColor
    view.layer.addSublayer(shapeLayer)

    return view
}

func ViewFromRectScreenMap(_ region: RectScreenMapRegion,
                           screenMapPointToCGPoint: (ScreenMapPoint) -> CGPoint,
                           color: UIColor?) -> UIView {
    let origin = screenMapPointToCGPoint(region.origin)
    let size = screenMapPointToCGPoint(region.size)

    let view = UIView(frame: CGRect(x: origin.x,
                                y: origin.y,
                                width: size.x,
                                height: size.y))

    view.backgroundColor = color ?? UIColor.black

    return view
}

func ViewFromScreenMap(_ region: ScreenMapRegion,
                       screenMapPointToCGPoint: (ScreenMapPoint) -> CGPoint,
                       color: UIColor) -> UIView? {
    if let rectRegion = region as? RectScreenMapRegion {
        return ViewFromRectScreenMap(rectRegion, screenMapPointToCGPoint: screenMapPointToCGPoint, color: color)
    }

    if let circleRegion = region as? CircleScreenMapRegion {
        return ViewFromCircleScreenMap(circleRegion, screenMapPointToCGPoint: screenMapPointToCGPoint, color: color)
    }

    return nil
}
