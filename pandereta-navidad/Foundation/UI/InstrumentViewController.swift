//
//  InstrumentViewController.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import UIKit
import CoreMotion

protocol InstrumentViewControllerDelegate {
    func instrumentDeviceMap() -> InstrumentDeviceMap?
    func instrumentViewController(_ viewController: InstrumentViewController, instrumentEvent: InstrumentEvent, intensity: Float)
    func shouldDrawRegions() -> Bool
    func backgroundColor(for event: InstrumentEvent) -> UIColor
}

let TouchPressureMaxIntensity: Float = 2.5
typealias MotionRecognizer = (Double) -> Bool
typealias AllAxisMotionRecognizer = (x: MotionRecognizer?, y: MotionRecognizer?, z: MotionRecognizer?)

let MotionGestureDetectorBuilder: (Double?, Double) -> MotionRecognizer = { initial, threshold in
    var previous = initial ?? 0
    var previousDirection = 0

    return { current in
        let old = previous
        previous = current

        let enoughMotionToGenerateAction = abs(old) + abs(current) > threshold
        if (old < 0 && current > 0) {
            previousDirection = 1

            return enoughMotionToGenerateAction
        }

        if (old > 0 && current < 0) {
            previousDirection = -1

            return enoughMotionToGenerateAction
        }

        return false
    }
}

let CGPointToScreenMapBuilder: (CGFloat, CGFloat) -> (CGPoint) -> ScreenMapPoint = { width, height in
    return { point in
        return ScreenMapPoint(x: Float(point.x/width), y: Float(point.y/height))
    }
}

let ScreenMapToCGPointBuilder: (CGFloat, CGFloat) -> (ScreenMapPoint) -> CGPoint = { width, height in
    return { point in
        return CGPoint(x: width * CGFloat(point.x), y: height * CGFloat(point.y))
    }
}

let TouchPressureToIntensity: (Float) -> (CGFloat) -> Float = { maxPressure in
    return { pressure in
        return Float(pressure)/maxPressure
    }
}

class InstrumentViewController: UIViewController {
    var delegate: InstrumentViewControllerDelegate?

    private var circles: [UITouch:(circle: Circle, location: CGPoint, intensity: CGFloat)] = [:]
    private let motionManager = CMMotionManager()
    private var shakeGestureRecognizer: ShakeGestureRecognizer?
    private var rotationGestureRecognizer: RotationGestureRecognizer?
    private let touchPressureConverter = TouchPressureToIntensity(TouchPressureMaxIntensity)
    private var CGPointToScreenMapPoint: ((CGPoint) -> ScreenMapPoint)!
    private var screenMapPointToCGPoint: ((ScreenMapPoint) -> CGPoint)!

    private var frameObserver: NSKeyValueObservation!

    init() {
        super.init(nibName: "InstrumentViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func shouldTrackRotation(instrumentDeviceMap: InstrumentDeviceMap) -> Bool {
        guard let instrumentDeviceMap = self.delegate?.instrumentDeviceMap() else {
            return false
        }

        return instrumentDeviceMap.shouldReceive(rotationEventsForAxis: .any)
    }

    func shouldTrackMotion(instrumentDeviceMap: InstrumentDeviceMap) -> Bool {
        guard let instrumentDeviceMap = self.delegate?.instrumentDeviceMap() else {
            return false
        }

        return instrumentDeviceMap.shouldReceive(shakeEventsForAxis: .any)
    }

    func setUpShakeUpdates(instrumentDeviceMap: InstrumentDeviceMap) {
        motionManager.accelerometerUpdateInterval = instrumentDeviceMap.shakeUpdatesInterval
        shakeGestureRecognizer = DefaultShakeGestureRecognizer(motionManager: self.motionManager)
        shakeGestureRecognizer!.delegate = self
        shakeGestureRecognizer!.starUpdates(forAxis: Array(instrumentDeviceMap.shakeMappings.keys))
    }

    func setUpRotationUpdates(instrumentDeviceMap: InstrumentDeviceMap) {
        motionManager.gyroUpdateInterval = instrumentDeviceMap.gyroUpdatesInterval
        rotationGestureRecognizer = DefaultRotationGestureRecognizer(motionManager: self.motionManager)
        rotationGestureRecognizer!.delegate = self
        rotationGestureRecognizer!.startUpdates(forAxis: Array(instrumentDeviceMap.rotationMappigns.keys))
    }

    func startMotionUpdatesIfNeeded() {
        if let instrumentDeviceMap = self.delegate?.instrumentDeviceMap() {
            if (shouldTrackRotation(instrumentDeviceMap: instrumentDeviceMap)) {
                setUpRotationUpdates(instrumentDeviceMap: instrumentDeviceMap)
            }

            if (shouldTrackMotion(instrumentDeviceMap: instrumentDeviceMap)) {
                setUpShakeUpdates(instrumentDeviceMap: instrumentDeviceMap)
            }
        }
    }

    func clearRegionsIfNeeded() {
//        if delegate?.shouldDrawRegions() ?? false {
//            _ = view.subviews.map { $0.removeFromSuperview() }
//        }
    }

    func drawRegionsIfNeeded() {
        if let delegate = delegate,
            delegate.shouldDrawRegions(),
            let instrumentDeviceMap = delegate.instrumentDeviceMap() {
            for (region, event) in instrumentDeviceMap.screenMappings.reversed() {
                if let regionView = ViewFromScreenMap(region,
                                                      screenMapPointToCGPoint: screenMapPointToCGPoint,
                                                      color: delegate.backgroundColor(for: event)) {
                    view.addSubview(regionView)
                    view.bringSubview(toFront: regionView)
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        startMotionUpdatesIfNeeded()
        frameObserver = self.observe(\.view.frame, changeHandler: { (_, _)  in
            self.updateView()
        })

        updateView()
    }

    func updateView() {
        let width = view.frame.width
        let height = view.frame.height
        screenMapPointToCGPoint = ScreenMapToCGPointBuilder(width, height)
        CGPointToScreenMapPoint = CGPointToScreenMapBuilder(width, height)

        clearRegionsIfNeeded()
        drawRegionsIfNeeded()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: view)
            let circle = Circle()
            // let _ = circle.draw(atPoint: location, touch: touch)
            let intensity = touch.force
            circles[touch] = (circle: circle, location: location, intensity: intensity)

//            view.layer.addSublayer(circle)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches where circles[touch] != nil {
            let (_, location, intensity) = circles[touch]!
            circles.removeValue(forKey: touch)

//            circle.removeFromSuperlayer()
            let frame = self.view.frame
            DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async {
                let point = ScreenMapPoint(Float(location.x), Float(location.y))

                if let delegate = self.delegate,
                    let instrumentDeviceMap = delegate.instrumentDeviceMap(),
                    let event = instrumentDeviceMap.find(eventForTouch: point, inRect: frame) {
                    delegate.instrumentViewController(self, instrumentEvent: event, intensity: Float(intensity)/TouchPressureMaxIntensity)
                }
            }
        }
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches where circles[touch] != nil {
            let circle = circles[touch]!.circle
            circles.removeValue(forKey: touch)

//            circle.removeFromSuperlayer()
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches where circles[touch] != nil {
            let (circle, location, _) = circles[touch]!

            //_ = circle.draw(atPoint: touch.location(in: view), touch: touch)
            let intensity = touch.force
            circles[touch] = (circle, location, intensity)

            if let instrumentDeviceMap = delegate?.instrumentDeviceMap(),
                let event = instrumentDeviceMap.find(eventForDrag: CGPointToScreenMapPoint(location)) {
                delegate?.instrumentViewController(self, instrumentEvent: event, intensity: Float(intensity)/TouchPressureMaxIntensity)
            }
        }
    }
}

extension InstrumentViewController: ShakeGestureRecognizerDelegate, RotationGestureRecognizerDelegate {
    func shakeRecognizerDidStop(_ recognizer: ShakeGestureRecognizer) {
    }

    func shakeRecognizerDidStart(_ recognizer: ShakeGestureRecognizer) {
    }

    func rotationRecognizerDidStop(_ recognizer: RotationGestureRecognizer) {
    }

    func rotationRecognizerDidStart(_ recognizer: RotationGestureRecognizer) {
    }

    func shakeRecognizer(_ recognizer: ShakeGestureRecognizer, didShakeInAxis axis: MotionAxis) {
        guard let instrumentDeviceMap = self.delegate?.instrumentDeviceMap(),
            let event = instrumentDeviceMap.findEvent(forShakeInAxis: axis) else {
                return
        }

        self.delegate!.instrumentViewController(self, instrumentEvent: event, intensity: 1)
    }

    func rotationRecognizer(_ recognizer: RotationGestureRecognizer, didRotateInAxis axis: MotionAxis) {
        guard let instrumentDeviceMap = self.delegate?.instrumentDeviceMap(),
            let event = instrumentDeviceMap.findEvent(forRotationInAxis: axis) else {
                return
        }

        self.delegate!.instrumentViewController(self, instrumentEvent: event, intensity: 1)
    }
}

class Circle: CAShapeLayer {

    var isMax: Bool = false {
        didSet {
            fillColor = isMax ? UIColor.yellow.cgColor : nil
        }
    }

    func draw(atPoint location: CGPoint, touch: UITouch) -> CGFloat {
        let radius = (touch.force * 75)

        path = UIBezierPath(ovalIn: CGRect(
            origin: location.offset(dx: radius, dy: radius),
            size: CGSize(width: radius * 2, height: radius * 2))).cgPath

        return touch.force
    }
}

extension CGPoint {
    func offset(dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPoint(x: self.x - dx, y: self.y - dy)
    }
}
