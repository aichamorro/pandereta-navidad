//
//  ScreenMapRegion.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit

protocol ScreenMapRegion {
    var x: Float { get }
    var y: Float { get }
    var size: ScreenMapPoint { get }

    func contains(_ point: ScreenMapPoint, inRect rect: CGRect) -> Bool
}

extension ScreenMapRegion {
    var origin: ScreenMapPoint {
        return ScreenMapPoint(x: x, y: y)
    }
}
