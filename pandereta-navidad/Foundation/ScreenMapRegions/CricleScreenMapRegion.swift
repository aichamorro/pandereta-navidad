//
//  CircleScreenMapRegion.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 05/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit

struct CircleScreenMapRegion: ScreenMapRegion {
    let isPointContained: (ScreenMapPoint, CGRect) -> Bool
    let x: Float
    let y: Float
    let radius: Float

    init(x: Float, y: Float, radius: Float) {
        self.x = x
        self.y = y
        self.radius = radius

        let r2 = radius * radius
        isPointContained = { point, rect in
            let (distX, distY) = (
                point.x - (x * Float(rect.width)),
                point.y - (y * Float(rect.height))
            )
            let (distX2, distY2) = (distX * distX, distY * distY)
            let radius2 = r2 * Float(rect.width) * Float(rect.width)

            return distX2 + distY2 <= radius2
        }
    }

    func contains(_ point: ScreenMapPoint, inRect rect: CGRect) -> Bool {
        return isPointContained(point, rect)
    }
}

extension CircleScreenMapRegion {
    var size: ScreenMapPoint {
        let diameter = 2 * radius

        return ScreenMapPoint(x: diameter, y: diameter)
    }
}
