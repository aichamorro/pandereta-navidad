//
//  RectScreenMapRegion.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 05/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit

private func assertValidPosition(x: Float, y: Float) -> Bool {
    return (x >= 0 && x <= 1 && y >= 0 && y <= 1)
}

private func assertValidSize(width: Float, height: Float) -> Bool {
    return (width > 0 && width <= 1 && height > 0 && height <= 1)
}

struct RectScreenMapRegion: ScreenMapRegion {
    let x: Float
    let y: Float
    let width: Float
    let height: Float

    init?(x: Float, y: Float, width: Float, height: Float) {
        if(!assertValidPosition(x: x, y: y) || !assertValidPosition(x: width, y: height)) {
            return nil
        }

        self.x = x
        self.y = y
        self.width = width
        self.height = height
    }

    func contains(_ point: ScreenMapPoint, inRect rect: CGRect) -> Bool {
        if (point.x < minX * Float(rect.width) || point.x > maxX * Float(rect.width)) {
            return false
        }

        if (point.y < minY * Float(rect.height) || point.y > maxY * Float(rect.height)) {
            return false
        }

        return true
    }
}

extension RectScreenMapRegion {

    var maxX: Float {
        return x + width
    }

    var maxY: Float {
        return y + height
    }

    var minX: Float {
        return x
    }

    var minY: Float {
        return y
    }

    var origin: ScreenMapPoint {
        return (x: x, y: y)
    }

    var size: ScreenMapPoint {
        return (x: width, y: height)
    }
}

extension RectScreenMapRegion {
    func updating(x: Float) -> RectScreenMapRegion {
        return RectScreenMapRegion(x: x, y: y, width: width, height: height)!
    }

    func updating(width: Float) -> RectScreenMapRegion {
        return RectScreenMapRegion(x: x, y: y, width: width, height: height)!
    }
}
