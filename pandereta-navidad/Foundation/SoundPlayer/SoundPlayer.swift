//
//  SoundPlayer.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 14/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import AudioKit

protocol SoundPlayer {
    var instrument: Instrument? { get set }
    func start()
    func stop()
}

class AKSoundPlayer: SoundPlayer {
    var instrument: Instrument?

    var output: AKNode? {
        get {
            return AudioKit.output
        }

        set {
            AudioKit.output = newValue
        }
    }

    func start() {
        guard let instrument = instrument else {
            return
        }

        instrument.load(with: self)
        AudioKit.start()
    }

    func stop() {
        AudioKit.stop()
    }
}
