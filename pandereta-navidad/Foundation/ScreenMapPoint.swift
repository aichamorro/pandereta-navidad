//
//  ScreenMapPoint.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

typealias ScreenMapPoint = (x: Float, y: Float)
