//
//  InstrumentEvent.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

typealias InstrumentEvent = (name: String, userInfo: [String: Any]?)

func InstrumentEventAreEquals(_ left: InstrumentEvent, _ right: InstrumentEvent) -> Bool {
    if (left.name != right.name) {
        return false
    }

    return true
}
