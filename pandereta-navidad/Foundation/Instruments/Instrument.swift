//
//  Instrument.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 13/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import AudioKit

protocol Instrument: class {
    func load(with soundPlayer: SoundPlayer)
    func play(event: InstrumentEvent, intensity: Float)
}

class AudioKitInstrument: Instrument {
    var instrumentSounds: [String: AKSamplePlayer] = [:]
    var mixerNode: AKMixer!

    func getSoundFilenames() -> [URL] {
        return []
    }

    func filename(for event: InstrumentEvent, intensity: Float) -> String? {
        return nil
    }

    final func load(with soundPlayer: SoundPlayer) {
        guard let soundPlayer = soundPlayer as? AKSoundPlayer else {
            return
        }

        let sounds: [URL] = getSoundFilenames()
        var players: [AKSamplePlayer] = []

        do {
            for filename in sounds {
                let audioFile = try AVAudioFile(forReading: filename)
                let samplePlayer = AKSamplePlayer(file: audioFile)

                players.append(samplePlayer)
                instrumentSounds[filename.lastPathComponent] = samplePlayer
            }
        } catch {
            AKLog(error.localizedDescription)
            AKLog("Files didn't load")
        }

        mixerNode = AKMixer(players)
        soundPlayer.output = mixerNode
    }

    final func play(event: InstrumentEvent, intensity: Float) {
        if let filename = filename(for: event, intensity: intensity),
            let sampler = instrumentSounds[filename] {
            sampler.play()
        }
    }
}
