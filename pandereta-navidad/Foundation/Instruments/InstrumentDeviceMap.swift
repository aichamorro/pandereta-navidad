//
//  InstrumentDeviceMap.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit
typealias RegionMapping = (ScreenMapRegion, InstrumentEvent)

enum MotionAxis {
    case x, y, z, any

    static var all: [MotionAxis] {
        return [.x, .y, .z]
    }
}

private let DefaultRotationUpdatesInterval: Double = 0.2
private let DefaultMotionUpdatesInterval: Double = 0.2

class InstrumentDeviceMap {
    let screenMappings: [RegionMapping]
    let shakeMappings: [MotionAxis: InstrumentEvent]
    let rotationMappigns: [MotionAxis: InstrumentEvent]
    let shakeUpdatesInterval: Double
    let gyroUpdatesInterval: Double

    init(regions: [RegionMapping],
         shakeMappings: [MotionAxis: InstrumentEvent] = [:],
         rotationMappings: [MotionAxis: InstrumentEvent] = [:],
         shakeUpdatesInterval: Double = DefaultMotionUpdatesInterval,
         gyroUpdatesInterval: Double = DefaultRotationUpdatesInterval) {
        self.screenMappings = regions
        self.shakeMappings = shakeMappings
        self.rotationMappigns = rotationMappings
        self.shakeUpdatesInterval = shakeUpdatesInterval
        self.gyroUpdatesInterval = gyroUpdatesInterval
    }

    func find(eventForTouch point: ScreenMapPoint, inRect rect: CGRect) -> InstrumentEvent? {
        return screenMappings.filter { $0.0.contains(point, inRect: rect) }.first?.1
    }

    func find(eventForDrag point: ScreenMapPoint) -> InstrumentEvent? {
        return nil
    }

    func findEvent(forShakeInAxis axis: MotionAxis) -> InstrumentEvent? {
        return shakeMappings[axis]
    }

    func findEvent(forRotationInAxis axis: MotionAxis) -> InstrumentEvent? {
        return rotationMappigns[axis]
    }

    func shouldReceive(shakeEventsForAxis axis: MotionAxis) -> Bool {
        return (.any == axis && shakeMappings.keys.count > 0) ?
            true :
            shakeMappings.keys.contains(axis)
    }

    func shouldReceive(rotationEventsForAxis axis: MotionAxis) -> Bool {
        return (.any == axis && rotationMappigns.keys.count > 0) ?
            true :
            rotationMappigns.keys.contains(axis)
    }
}

extension InstrumentDeviceMap {
    static func Builder() -> InstrumentDeviceMapBuilder {
        return InstrumentDeviceMapBuilder()
    }
}

class InstrumentDeviceMapBuilder {
    private var regions: [RegionMapping] = []
    private var shakeEvents: [MotionAxis: InstrumentEvent] = [:]
    private var rotationEvents: [MotionAxis: InstrumentEvent] = [:]

    func add(region: ScreenMapRegion, event: InstrumentEvent) -> InstrumentDeviceMapBuilder {
        regions.append(RegionMapping(region, event))

        return self
    }

    func add(shakeEvent: InstrumentEvent, forAxis axis: MotionAxis) -> InstrumentDeviceMapBuilder {
        shakeEvents[axis] = shakeEvent

        return self
    }

    func add(rotationEvent: InstrumentEvent, forAxis axis: MotionAxis) -> InstrumentDeviceMapBuilder {
        rotationEvents[axis] = rotationEvent

        return self
    }

    func build() -> InstrumentDeviceMap {
        return InstrumentDeviceMap(regions: regions, shakeMappings: shakeEvents, rotationMappings: rotationEvents)
    }
}
