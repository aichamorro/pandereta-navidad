//
//  ShakeGestureRecognizer.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 03/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import CoreMotion

protocol ShakeGestureRecognizer {
    weak var delegate: ShakeGestureRecognizerDelegate? { get set }

    func starUpdates(forAxis axis: [MotionAxis])
    func stopUpdates()
}

protocol ShakeGestureRecognizerDelegate: class {
    func shakeRecognizerDidStart(_ recognizer: ShakeGestureRecognizer)
    func shakeRecognizerDidStop(_ recognizer: ShakeGestureRecognizer)
    func shakeRecognizer(_ recognizer: ShakeGestureRecognizer, didShakeInAxis axis: MotionAxis)
}

class DefaultShakeGestureRecognizer: ShakeGestureRecognizer {
    let motionManager: CMMotionManager
    weak var delegate: ShakeGestureRecognizerDelegate?

    init(motionManager: CMMotionManager) {
        self.motionManager = motionManager
    }

    func getInitialData() -> (x: Double, y: Double, z: Double) {
        return { () -> (x: Double, y: Double, z: Double) in
            guard let data = motionManager.accelerometerData?.acceleration else {
                return (x: 0.0, y: 0.0, z: 0.0)
            }

            return (x: data.x, y: data.y, z: data.z)
        }()
    }

    func starUpdates(forAxis axis: [MotionAxis]) {
        let accelerationRecognizers = BuildMotionRecognizers(threshold: 0.2, axis: axis, initialData: getInitialData())

        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            self.handleAccelerometerUpdates(data: data, error: error, motionRecognizers: accelerationRecognizers)
        }

        delegate?.shakeRecognizerDidStart(self)
    }

    func stopUpdates() {
        motionManager.stopAccelerometerUpdates()
        delegate?.shakeRecognizerDidStop(self)
    }

    func handleAccelerometerUpdates(data: CMAccelerometerData?, error: Error?, motionRecognizers: AllAxisMotionRecognizer) {
        guard error == nil else {
            NSLog("Error: \(error!.localizedDescription)")

            return
        }

        guard let data = data else {
            NSLog("No gyro data has been received!")

            return
        }

        if (motionRecognizers.x?(data.acceleration.x) ?? false) {
            didShake(inAxis: .x)
        }

        if (motionRecognizers.y?(data.acceleration.y) ?? false) {
            didShake(inAxis: .y)
        }

        if (motionRecognizers.z?(data.acceleration.z) ?? false) {
            didShake(inAxis: .z)
        }
    }

    func didShake(inAxis axis: MotionAxis) {
        self.delegate?.shakeRecognizer(self, didShakeInAxis: axis)
    }
}
