//
//  Common.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 03/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

func BuildMotionRecognizers(threshold: Double,
                            axis: [MotionAxis],
                            initialData: (x: Double, y: Double, z: Double)) -> (x: MotionRecognizer?, y: MotionRecognizer?, z: MotionRecognizer?) {
    var xAxis: MotionRecognizer?, yAxis: MotionRecognizer?, zAxis: MotionRecognizer?

    if (axis.contains(.x)) {
        xAxis = MotionGestureDetectorBuilder(initialData.x, threshold)
    }
    if (axis.contains(.y)) {
        yAxis = MotionGestureDetectorBuilder(initialData.y, threshold)
    }
    if (axis.contains(.z)) {
        zAxis = MotionGestureDetectorBuilder(initialData.z, threshold)
    }

    return (x: xAxis, y: yAxis, z: zAxis)
}
