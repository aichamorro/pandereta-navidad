//
//  RotationGestureRecognizer.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 03/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import CoreMotion

protocol RotationGestureRecognizer {
    weak var delegate: RotationGestureRecognizerDelegate? { get set }

    func startUpdates(forAxis axis: [MotionAxis])
    func stopUpdates()
}

protocol RotationGestureRecognizerDelegate: class {
    func rotationRecognizerDidStart(_ recognizer: RotationGestureRecognizer)
    func rotationRecognizerDidStop(_ recognizer: RotationGestureRecognizer)
    func rotationRecognizer(_ recognizer: RotationGestureRecognizer, didRotateInAxis axis: MotionAxis)
}

class DefaultRotationGestureRecognizer: RotationGestureRecognizer {
    let motionManager: CMMotionManager
    weak var delegate: RotationGestureRecognizerDelegate?

    init(motionManager: CMMotionManager) {
        self.motionManager = motionManager
    }

    private func getInitialData() -> (x: Double, y: Double, z: Double) {
        return { () -> (x: Double, y: Double, z: Double) in
            guard let data = motionManager.gyroData?.rotationRate else {
                return (x: 0.0, y: 0.0, z: 0.0)
            }

            return (x: data.x, y: data.y, z: data.z)
        }()
    }

    func startUpdates(forAxis axis: [MotionAxis]) {
        let gyroRecognizers = BuildMotionRecognizers(threshold: 0.2, axis: axis, initialData: getInitialData())

        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data, error) in
            self.handleGyroUpdate(data: data, error: error, motionRecognizers: gyroRecognizers)
        }

        delegate?.rotationRecognizerDidStart(self)
    }

    func stopUpdates() {
        motionManager.stopGyroUpdates()
        delegate?.rotationRecognizerDidStop(self)
    }

    func handleGyroUpdate(data: CMGyroData?, error: Error?, motionRecognizers: AllAxisMotionRecognizer) {
        guard error == nil else {
            NSLog("Error: \(error!.localizedDescription)")

            return
        }

        guard let data = data else {
            NSLog("No gyro data has been received!")

            return
        }

        if (motionRecognizers.x?(data.rotationRate.x) ?? false) {
            didRotate(inAxis: .x)
        }

        if (motionRecognizers.y?(data.rotationRate.y) ?? false) {
            didRotate(inAxis: .y)
        }

        if (motionRecognizers.z?(data.rotationRate.z) ?? false) {
            didRotate(inAxis: .z)
        }
    }

    func didRotate(inAxis axis: MotionAxis) {
        self.delegate?.rotationRecognizer(self, didRotateInAxis: axis)
    }

}
