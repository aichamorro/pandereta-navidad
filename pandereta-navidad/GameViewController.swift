//
//  GameViewController.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit

protocol GameViewControllerDelegate: class {
    func viewControllerForInstrument() -> UIViewController
    func viewControllerForTimeline() -> UIViewController
    func heightForTimeline() -> CGFloat
    func ready()
}

private let StatusBarHeight: CGFloat = 44

class GameViewController: UIViewController {
    var instrumentViewController: UIViewController?
    var timelineViewController: UIViewController?
    var delegate: GameViewControllerDelegate?

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let timeline = setUpTimeline() {
            timelineViewController = timeline
        }

        if let instrument = setUpInstrumentController() {
            instrumentViewController = instrument
        }
    }

    func setUpInstrumentController() -> UIViewController? {
        var result: UIViewController?

        if let delegate = delegate {
            let yOrigin = timelineViewController?.view.frame.height ?? 0
            let instrumentControllerFrame = CGRect(x: 0, y: yOrigin, width: view.frame.size.width, height: view.frame.size.height - yOrigin)
            result = delegate.viewControllerForInstrument()
            result!.view.frame = instrumentControllerFrame

            view.addSubview(result!.view)
            addChildViewController(result!)
            result?.didMove(toParentViewController: self)
        }

        return result
    }

    func setUpTimeline() -> UIViewController? {
        var result: UIViewController?

        if let delegate = delegate {
            result = delegate.viewControllerForTimeline()

            let timelineView = result!.view!
            let timelineViewFrame = CGRect(x: 0, y: StatusBarHeight, width: view.frame.size.width, height: delegate.heightForTimeline())
            timelineView.frame = timelineViewFrame
            timelineView.backgroundColor = UIColor.red

            view.addSubview(result!.view)
            addChildViewController(result!)
            result?.didMove(toParentViewController: self)
        }

        return result
    }
}
