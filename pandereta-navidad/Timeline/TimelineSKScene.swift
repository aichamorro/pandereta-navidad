//
//  TimelineSKScene.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import SpriteKit

private var squareSize = CGSize(width: 30, height: 20)
private let secondsToMinutes = Double(1.0/60.0)
private let hitlineWidth: CGFloat = 2
private let offset: CGFloat = 20

class TimelineSKScene: SKScene {
    var bpm: Float!
    var beatsPerBar: Int!
    var numberOfBars: Int!
    var mainNode: SKNode!
    var gridNode: GridNode!

    var durationInBeats: Int! {
        return beatsPerBar * numberOfBars
    }
    private var totalTime: TimeInterval {
        return Double(Float(durationInBeats) / bpm) * 60.0
    }

    var startTime: TimeInterval = Double(-MAXFLOAT)

    override func didMove(to view: SKView) {
        super.didMove(to: view)

        // initialize
        backgroundColor = UIColor.white
        self.size = view.frame.size
    }

    func addGrid() {
        let rows = 6
        squareSize.height = size.height/CGFloat(rows)
        gridNode = GridNode(squareSize: squareSize, rows: rows, beatsPerBar: beatsPerBar, numBars: numberOfBars)!

        gridNode.anchorPoint = CGPoint(x: 0, y: 0)
        gridNode.position.x = offset
        gridNode.zPosition = -2.0

        mainNode.addChild(gridNode)
    }

    func horizontalOffset(forBeatIn time: TimeInterval) -> CGFloat {
        return CGFloat(horizontalOffset(forBeat: beat(forTime: time)))
    }

    func horizontalOffset(forBeat beat: Double) -> CGFloat {
        return CGFloat(beat/Double(durationInBeats)) * size.width
    }

    func beat(forTime time: TimeInterval) -> Double {
        return time * Double(secondsToMinutes) * Double(bpm)
    }

    func willAppear() {
        mainNode = SKNode()
        addChild(mainNode)

        addGrid()
        addHitLine()
    }

    func addHitLine() {
        let hitLine = HitLineNode(position: CGPoint(x: offset, y: 0),
                                  size: CGSize(width: hitlineWidth, height: size.height))

        addChild(hitLine)
    }

    func addTag(location: Double, note: Note) {
        gridNode.addTag(location: location, squareSize: squareSize)
    }

    override func update(_ currentTime: TimeInterval) {
        if startTime <= 0 {
            startTime = currentTime
        }

        let completed = CGFloat((currentTime - startTime)/totalTime)
        mainNode.position.x = -(completed * gridNode.frame.size.width)
    }
}
