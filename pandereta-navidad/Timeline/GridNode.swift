//
//  GridNode.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 09/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import SpriteKit

class GridNode: SKSpriteNode {

    convenience init?(squareSize: CGSize, rows: Int, beatsPerBar: Int, numBars: Int) {
        guard let texture = GridNode.gridTexture(squareSize: squareSize, rows: rows, beatsPerBar: beatsPerBar, numBars: numBars) else {
            return nil
        }

        self.init(texture: texture, color: SKColor.clear, size: texture.size())
        let barSeparationsTexture = GridNode.barSeparations(squareSize: squareSize, rows: rows, beatsPerBar: beatsPerBar, numBars: numBars)!
        let barSeparations = SKSpriteNode(texture: barSeparationsTexture)
        barSeparations.anchorPoint = CGPoint.zero
        addChild(barSeparations)
    }

    func addTag(location: Double, squareSize: CGSize) {
        let rect = CGRect(x: CGFloat(location) * size.width,
                          y: 2 * squareSize.height,
                          width: squareSize.width,
                          height: squareSize.height)
        let tag = TagNode(position: rect.origin, size: rect.size)

        addChild(tag)
    }

    class func barSeparations(squareSize: CGSize, rows: Int, beatsPerBar: Int, numBars: Int) -> SKTexture? {
        let size =  CGSize(width: squareSize.width * CGFloat(beatsPerBar * numBars),
                           height: squareSize.height * CGFloat(rows))

        UIGraphicsBeginImageContext(size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }

        let barSeparation = UIBezierPath()
        let columnsToDraw = (0..<numBars)
        columnsToDraw.map {
            CGFloat($0 * beatsPerBar) * squareSize.width
        }.forEach {
            barSeparation.move(to: CGPoint(x: $0, y: 0))
            barSeparation.addLine(to: CGPoint(x: $0, y: size.height))
        }

        barSeparation.lineWidth = 1.0
        UIColor.red.withAlphaComponent(0.8).setStroke()
        barSeparation.stroke()
        context.addPath(barSeparation.cgPath)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return SKTexture(image: image!)
    }

    class func gridTexture(squareSize: CGSize, rows: Int, beatsPerBar: Int, numBars: Int) -> SKTexture? {
        let size =  CGSize(width: squareSize.width * CGFloat(beatsPerBar * numBars),
                           height: squareSize.height * CGFloat(rows))

        UIGraphicsBeginImageContext(size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }

        let bezierPath = UIBezierPath()
        let columnsToDraw = (0..<beatsPerBar * numBars)
        columnsToDraw.map {
            CGFloat($0) * squareSize.width
        }.forEach {
            bezierPath.move(to: CGPoint(x: $0, y: 0))
            bezierPath.addLine(to: CGPoint(x: $0, y: size.height))
        }

        (0..<rows)
            .map { CGFloat($0) * squareSize.height }
            .forEach {
                bezierPath.move(to: CGPoint(x: 0, y: $0))
                bezierPath.addLine(to: CGPoint(x: size.width, y: $0))
            }

        bezierPath.lineWidth = 1.0
        UIColor.black.withAlphaComponent(0.2).setStroke()
        bezierPath.stroke()
        context.addPath(bezierPath.cgPath)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return SKTexture(image: image!)
    }
}
