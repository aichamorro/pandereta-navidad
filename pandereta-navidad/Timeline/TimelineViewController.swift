//
//  TimelineViewController.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

protocol GameTimeline {
    func start()
    func stop()
}

private let DefaultBpm: Float = 120

class TimelineViewController: UIViewController, GameTimeline {
    var timelineScene: TimelineSKScene!
    let scoreProvider: TimelineScoreProvider
    var spriteView: SKView {
        return view as! SKView
    }

    init(scoreProvider: TimelineScoreProvider) {
        self.scoreProvider = scoreProvider

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = SKView()
    }

    override func viewDidLoad() {
        timelineScene = TimelineSKScene()
    }

    override func viewWillAppear(_ animated: Bool) {
        timelineScene.size = view.frame.size

        let score = scoreProvider.read()
        timelineScene.beatsPerBar = score.beatsPerBar
        timelineScene.numberOfBars = score.bars

        timelineScene.bpm = DefaultBpm

        timelineScene.willAppear()
        score.data.forEach { timelineScene.addTag(location: $0.key, note: $0.value) }

        spriteView.presentScene(timelineScene)
    }

    func start() {
    }

    func stop() {
    }
}
