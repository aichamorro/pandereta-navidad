//
//  TagNode.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import SpriteKit

class TagNode: SKShapeNode {
    convenience init(position: CGPoint, size: CGSize) {
        self.init(rect: CGRect(origin: position, size: size))
        strokeColor = UIColor.black.withAlphaComponent(0.7)
        fillColor = UIColor.red
        lineWidth = 0.5

        self.name = "Tag"
    }
}
