//
//  TimelineScoreProvider.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

protocol TimelineScoreProvider {
    func read() -> TimelineScore
}

class DummyTimelineScoreProvider: TimelineScoreProvider {
    func read() -> TimelineScore {
        let bars = 15
        let beatsPerBar = 4
        let durationInBeats = Double(bars * beatsPerBar)

        let notes: [Double: Note] =
            (0..<bars) // note in time (s)
                .map { [Double(Double($0 * beatsPerBar)/durationInBeats): Note()] } // note in % of the song
                .flatMap { $0 }
                .reduce(into: [Double: Note]()) { (result: inout [Double: Note], entry: (key: Double, value: Note)) in
                    result[entry.key] = entry.value
                }

        return TimelineScore(bars: bars, beatsPerBar: beatsPerBar, data: notes)
    }
}
