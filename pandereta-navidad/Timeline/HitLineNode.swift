//
//  HitLineNode.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import SpriteKit

class HitLineNode: SKShapeNode {
    convenience init(position: CGPoint, size: CGSize) {
        self.init(rect: CGRect(origin: position, size: size))
        strokeColor = UIColor(red: 255/255, green: 102/255, blue: 0, alpha: 0.8)
        fillColor = UIColor.orange.withAlphaComponent(0.5)
    }
}
