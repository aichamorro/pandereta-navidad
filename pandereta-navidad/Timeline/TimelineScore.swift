//
//  TimelineScore.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 08/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

struct TimelineScore {
    let bars: Int
    let beatsPerBar: Int
    let data: [TimeInterval: Note]
}

extension TimelineScore {
    var beats: Int {
        return bars * beatsPerBar
    }
}
