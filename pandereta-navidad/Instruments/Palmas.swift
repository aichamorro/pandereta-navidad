//
//  Palmas.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit

enum PalmasInstrumentEvents: String {
    case Abierta
    case Sorda
}

func InstrumentDeviceMapPalmas() -> InstrumentDeviceMap {
    let abierta = RectScreenMapRegion(x: 0, y: 0, width: 1, height: 0.5)!
    let sorda = RectScreenMapRegion(x: 0, y: abierta.maxY, width: 1, height: 0.5)!
    let test = CircleScreenMapRegion(x: 0.5, y: 0.5, radius: 0.25)

    return InstrumentDeviceMap.Builder()
        .add(region: test, event: InstrumentEvent(name: PalmasInstrumentEvents.Sorda.rawValue, nil))
        .add(region: abierta, event: InstrumentEvent(name: PalmasInstrumentEvents.Abierta.rawValue, nil))
        .add(region: sorda, event: InstrumentEvent(name: PalmasInstrumentEvents.Abierta.rawValue, nil))
//        .add(region: sorda, event: InstrumentEvent(name: PalmasInstrumentEvents.Sorda.rawValue, nil))
        .add(rotationEvent: InstrumentEvent(name: PalmasInstrumentEvents.Abierta.rawValue, nil), forAxis: .x)
        .add(shakeEvent: InstrumentEvent(name: PalmasInstrumentEvents.Sorda.rawValue, nil), forAxis: .y)
        .build()
}

func ColorMapPalmas(forEvent event: InstrumentEvent) -> UIColor {
    switch event.name {
    case PalmasInstrumentEvents.Abierta.rawValue: return UIColor.blue
    case PalmasInstrumentEvents.Sorda.rawValue: return UIColor.brown
    default: return UIColor.red
    }
}
