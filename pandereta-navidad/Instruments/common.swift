//
//  common.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 17/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

let nextRandomIndexGenerator: (Int) -> Int = { items in
    let numberOfItems = UInt32(items)

    return Int(arc4random_uniform(numberOfItems))
}

let nextSoundGenerator: (Int, Int, [URL]) -> () -> String = { startIndex, endIndex, sounds in
    let numberOfItems = endIndex - startIndex

    return {
        let nextIndex = nextRandomIndexGenerator(numberOfItems)
        let sound = sounds[nextIndex + startIndex].lastPathComponent

//        NSLog(sound)
        return sound
    }
}
