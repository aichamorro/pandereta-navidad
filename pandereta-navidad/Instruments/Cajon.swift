//
//  Cajon.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 12/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import UIKit

enum CajonInstrumentEvents: String {
    case AgudoSeco
    case AgudoCentral
    case Agudo
    case Grave
}

func InstrumentDeviceMapCajon() -> InstrumentDeviceMap {
    let agudoSecoIzq = RectScreenMapRegion(x: 0, y: 0, width: 0.17, height: 0.08)!
    let agudoSecoCentral = agudoSecoIzq.updating(x: agudoSecoIzq.maxX).updating(width: 1 - 2 * agudoSecoIzq.width)
    let agudoSecoDerecha = agudoSecoIzq.updating(x: agudoSecoCentral.maxX)
    let agudoNormal = RectScreenMapRegion(x: 0, y: agudoSecoIzq.height, width: 1, height: 0.30)!
    let grave = RectScreenMapRegion(x: 0, y: agudoNormal.height, width: 1, height: 0.35)!

    return InstrumentDeviceMap.Builder()
        .add(region: agudoSecoIzq, event: InstrumentEvent(name: CajonInstrumentEvents.AgudoSeco.rawValue, nil))
        .add(region: agudoSecoDerecha, event: InstrumentEvent(name: CajonInstrumentEvents.AgudoSeco.rawValue, nil))
        .add(region: agudoSecoCentral, event: InstrumentEvent(name: CajonInstrumentEvents.AgudoCentral.rawValue, nil))
        .add(region: agudoNormal, event: InstrumentEvent(name: CajonInstrumentEvents.Agudo.rawValue, nil))
        .add(region: grave, event: InstrumentEvent(name: CajonInstrumentEvents.Grave.rawValue, nil))
        .build()
}

func ColorMapCajon(forEvent event: InstrumentEvent) -> UIColor {
    switch event.name {
    case CajonInstrumentEvents.AgudoSeco.rawValue: return UIColor.blue
    case CajonInstrumentEvents.AgudoCentral.rawValue: return UIColor.green
    case CajonInstrumentEvents.Agudo.rawValue: return UIColor.brown
    case CajonInstrumentEvents.Grave.rawValue: return UIColor.red
    default:
        return UIColor.black
    }
}
