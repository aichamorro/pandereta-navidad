//
//  PalmasInstrument.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro - Personal on 13/11/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

private let AllSounds = [
    R.file.sorda_bajoAiff()!,
    R.file.sorda_medio_bajoAiff()!,
    R.file.sorda_medioAiff()!,
    R.file.sorda_medio_altoAiff()!,
    R.file.sorda_altoAiff()!,
    R.file.sorda_maxAiff()!,
    R.file.palmaAiff()!
]

class PalmasInstrument: AudioKitInstrument {
    override func getSoundFilenames() -> [URL] {
        return AllSounds
    }

    override func filename(for event: InstrumentEvent, intensity: Float) -> String? {
        switch event.name {
        case PalmasInstrumentEvents.Sorda.rawValue:
            return intensity < 0.25 ? R.file.sorda_bajoAiff()?.lastPathComponent :
            intensity < 0.4 ? R.file.sorda_medio_bajoAiff()?.lastPathComponent:
            intensity < 0.65 ? R.file.sorda_medioAiff()?.lastPathComponent :
            intensity < 0.85 ? R.file.sorda_medio_altoAiff()?.lastPathComponent :
            intensity < 0.95 ? R.file.sorda_altoAiff()?.lastPathComponent :
            R.file.sorda_maxAiff()?.lastPathComponent

        case PalmasInstrumentEvents.Abierta.rawValue:
            return R.file.palmaAiff()?.lastPathComponent

        default:
            return nil
        }
    }
}
