//
//  Tambourine.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 16/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation
import UIKit.UIColor

enum TambourineInstrumentEvents: String {
    case Center
    case OuterRadius
    case InnerRadius
    case Jingles
}

extension TambourineInstrumentEvents {
    var instrumentEvent: InstrumentEvent {
        let name: String = self.rawValue

        return InstrumentEvent(name: name, userInfo: nil)
    }
}

func InstrumentDeviceMapTambourine() -> InstrumentDeviceMap {
    let tambourineInteractiveRegion: (x: Float, y: Float, radius: Float) = (x: 0.645, y: 0.503, radius: 0.513)

    let outerRadius = CircleScreenMapRegion(x: tambourineInteractiveRegion.x,
                                            y: tambourineInteractiveRegion.y,
                                            radius: tambourineInteractiveRegion.radius)
    let innerRadius = CircleScreenMapRegion(x: tambourineInteractiveRegion.x,
                                            y: tambourineInteractiveRegion.y,
                                            radius: tambourineInteractiveRegion.radius * 0.75)
    let center = CircleScreenMapRegion(x: tambourineInteractiveRegion.x,
                                       y: tambourineInteractiveRegion.y,
                                       radius: tambourineInteractiveRegion.radius * 0.20)

    let jingleTopLeft = CircleScreenMapRegion(x: 0.31, y: 0.285, radius: 0.14)
    let jingleTopRight = CircleScreenMapRegion(x: 0.97, y: 0.281, radius: 0.125)
    let jingleTop = CircleScreenMapRegion(x: 0.635, y: 0.21, radius: 0.125)
    let jingleBottom = CircleScreenMapRegion(x: 0.655, y: 0.78, radius: 0.14)
    let jingleLeft = CircleScreenMapRegion(x: 0.142, y: 0.484, radius: 0.14)
    let jingleBottomLeft = CircleScreenMapRegion(x: 0.253, y: 0.686, radius: 0.14)
    let jingleBottomRight = CircleScreenMapRegion(x: 0.975, y: 0.67, radius: 0.14)

    return InstrumentDeviceMap.Builder()
        .add(region: center, event: TambourineInstrumentEvents.Center.instrumentEvent)
        .add(region: innerRadius, event: TambourineInstrumentEvents.InnerRadius.instrumentEvent)
        .add(region: outerRadius, event: TambourineInstrumentEvents.OuterRadius.instrumentEvent)
        .add(region: jingleTopLeft, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .add(region: jingleTopRight, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .add(region: jingleTop, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .add(region: jingleBottom, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .add(region: jingleLeft, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .add(region: jingleBottomLeft, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .add(region: jingleBottomRight, event: TambourineInstrumentEvents.Jingles.instrumentEvent)
        .build()
}

func ColorMapTambourine(forEvent event: InstrumentEvent) -> UIColor {
    switch event.name {
    case TambourineInstrumentEvents.Center.rawValue: return UIColor.blue.withAlphaComponent(0.3)
    case TambourineInstrumentEvents.OuterRadius.rawValue: return UIColor.brown.withAlphaComponent(0.3)
    case TambourineInstrumentEvents.InnerRadius.rawValue: return UIColor.red.withAlphaComponent(0.3)
    case TambourineInstrumentEvents.Jingles.rawValue: return UIColor.purple.withAlphaComponent(0.3)
    default: return UIColor.red
    }
}
