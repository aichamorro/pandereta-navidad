//
//  TambourineSoundSelector.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 17/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

private struct Limits {
    static let Low = 0.1
    static let MedLow = 0.2
    static let Med = 0.7
    static let MedHigh = 0.9
}

typealias SoundGenerator = () -> String
typealias SoundIntensityGenerator = (
    @escaping SoundGenerator,
    @escaping SoundGenerator,
    @escaping SoundGenerator,
    @escaping SoundGenerator,
    @escaping SoundGenerator) -> (Float) -> String

private let soundForIntensityGenerator: SoundIntensityGenerator = {
    (low: @escaping SoundGenerator,
    low_med: @escaping SoundGenerator,
    med: @escaping SoundGenerator,
    med_high: @escaping SoundGenerator,
    high: @escaping SoundGenerator) in

    return { intensity in
        if intensity < 0.1 {
            return low()
        }

        if intensity < 0.2 {
            return low_med()
        }

        if intensity < 0.7 {
            return med()
        }

        if intensity < 0.9 {
            return med_high()
        }

        return high()
    }
}

private let nextCenterHighSound: () -> String = nextSoundGenerator(0, 6, TambourineSounds)
private let nextCenterLowSound: () -> String = nextSoundGenerator(7, 9, TambourineSounds)
private let nextCenterLowMedSound: () -> String = nextSoundGenerator(10, 12, TambourineSounds)
private let nextCenterMedSound: () -> String = nextSoundGenerator(13, 17, TambourineSounds)
private let nextCenterMedHighSound: () -> String = nextSoundGenerator(18, 20, TambourineSounds)
let nextCenterSoundForIntensity: (Float) -> String = soundForIntensityGenerator(nextCenterLowSound,
                                                                             nextCenterLowMedSound,
                                                                             nextCenterMedSound,
                                                                             nextCenterMedHighSound,
                                                                             nextCenterHighSound)

private let nextInnerHighSound: () -> String = nextSoundGenerator(21, 29, TambourineSounds)
private let nextInnerLowSound: () -> String = nextSoundGenerator(30, 35, TambourineSounds)
private let nextInnerLowMedSound: () -> String = nextSoundGenerator(36, 38, TambourineSounds)
private let nextInnerMedSound: () -> String = nextSoundGenerator(39, 41, TambourineSounds)
private let nextInnerHighMed: () -> String = nextSoundGenerator(42, 45, TambourineSounds)
let nextInnerSoundForIntensity: (Float) -> String = soundForIntensityGenerator(nextInnerLowSound,
                                                                            nextInnerLowMedSound,
                                                                            nextInnerMedSound,
                                                                            nextInnerHighMed,
                                                                            nextInnerHighSound)

private let nextOuterHighSound: () -> String = nextSoundGenerator(46, 51, TambourineSounds)
private let nextOuterLowSound: () -> String = nextSoundGenerator(52, 59, TambourineSounds)
private let nextOuterLowMedSound: () -> String = nextSoundGenerator(60, 65, TambourineSounds)
private let nextOuterMedSound: () -> String = nextSoundGenerator(66, 69, TambourineSounds)
private let nextOuterHighMed: () -> String = nextSoundGenerator(70, 72, TambourineSounds)
let nextOuterSoundForIntensity: (Float) -> String = soundForIntensityGenerator(nextOuterLowSound,
                                                                               nextOuterLowMedSound,
                                                                               nextOuterMedSound,
                                                                               nextOuterHighMed,
                                                                               nextOuterHighSound)

private let nextJingleHighSound: () -> String = nextSoundGenerator(73, 79, TambourineSounds)
private let nextJingleLowSound: () -> String = nextSoundGenerator(80, 85, TambourineSounds)
private let nextJingleLowMedSound: () -> String = nextSoundGenerator(86, 87, TambourineSounds)
private let nextJingleMedSound: () -> String = nextSoundGenerator(88, 96, TambourineSounds)
private let nextJingleHighMed: () -> String = nextSoundGenerator(97, 99, TambourineSounds)
let nextJingleSoundForIntensity: (Float) -> String = soundForIntensityGenerator(nextJingleLowSound,
                                                                               nextJingleLowMedSound,
                                                                               nextJingleMedSound,
                                                                               nextJingleHighMed,
                                                                               nextJingleHighSound)
