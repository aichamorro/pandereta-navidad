//
//  TambourineSounds.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 17/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

let TambourineSounds = [
    R.file.centerHigh1Aiff()!,
    R.file.centerHigh2Aiff()!,
    R.file.centerHigh3Aiff()!,
    R.file.centerHigh4Aiff()!,
    R.file.centerHigh5Aiff()!,
    R.file.centerHigh7Aiff()!,
    R.file.centerHigh8Aiff()!,
    R.file.centerLow4Aiff()!, // 7
    R.file.centerLow5Aiff()!,
    R.file.centerLow6Aiff()!,
    R.file.centerLowMed3Aiff()!, // 10
    R.file.centerLowMed4Aiff()!,
    R.file.centerLowMed5Aiff()!,
    R.file.centerMed2Aiff()!, // 13
    R.file.centerMed3Aiff()!,
    R.file.centerMed4Aiff()!,
    R.file.centerMed5Aiff()!,
    R.file.centerMed6Aiff()!,
    R.file.centerMedHigh2Aiff()!, // 18
    R.file.centerMedHigh3Aiff()!,
    R.file.centerMedHigh4Aiff()!,
    R.file.innerHigh1Aiff()!, // 21
    R.file.innerHigh2Aiff()!,
    R.file.innerHigh3Aiff()!,
    R.file.innerHigh4Aiff()!,
    R.file.innerHigh5Aiff()!,
    R.file.innerHigh6Aiff()!,
    R.file.innerHigh7Aiff()!,
    R.file.innerHigh8Aiff()!,
    R.file.innerHigh9Aiff()!,
    R.file.innerLow2Aiff()!, // 30
    R.file.innerLow3Aiff()!,
    R.file.innerLow4Aiff()!,
    R.file.innerLow5Aiff()!,
    R.file.innerLow6Aiff()!,
    R.file.innerLow7Aiff()!,
    R.file.innerLowMed1Aiff()!, // 36
    R.file.innerLowMed2Aiff()!,
    R.file.innerLowMed3Aiff()!,
    R.file.innerMed1Aiff()!, // 39
    R.file.innerMed2Aiff()!,
    R.file.innerMed3Aiff()!,
    R.file.innerMedHigh1Aiff()!, // 42
    R.file.innerMedHigh2Aiff()!,
    R.file.innerMedHigh3Aiff()!,
    R.file.innerMedHigh4Aiff()!,
    R.file.outerHigh1Aiff()!, // 46
    R.file.outerHigh2Aiff()!,
    R.file.outerHigh4Aiff()!,
    R.file.outerHigh5Aiff()!,
    R.file.outerHigh6Aiff()!,
    R.file.outerHigh8Aiff()!,
    R.file.outerLow1Aiff()!, // 52
    R.file.outerLow10Aiff()!,
    R.file.outerLow3Aiff()!,
    R.file.outerLow4Aiff()!,
    R.file.outerLow5Aiff()!,
    R.file.outerLow6Aiff()!,
    R.file.outerLow7Aiff()!,
    R.file.outerLow9Aiff()!,
    R.file.outerLowMed1Aiff()!, // 60
    R.file.outerLowMed2Aiff()!,
    R.file.outerLowMed3Aiff()!,
    R.file.outerLowMed4Aiff()!,
    R.file.outerLowMed5Aiff()!,
    R.file.outerLowMed6Aiff()!,
    R.file.outerMed1Aiff()!, // 66
    R.file.outerMed2Aiff()!,
    R.file.outerMed3Aiff()!,
    R.file.outerMed4Aiff()!,
    R.file.outerMedHigh1Aiff()!, // 70
    R.file.outerMedHigh3Aiff()!,
    R.file.outerMedHigh4Aiff()!,
    R.file.jingleHigh1Aiff()!, // 73
    R.file.jingleHigh2Aiff()!,
    R.file.jingleHigh3Aiff()!,
    R.file.jingleHigh4Aiff()!,
    R.file.jingleHigh5Aiff()!,
    R.file.jingleHigh6Aiff()!,
    R.file.jingleHigh7Aiff()!,
    R.file.jingleLow2Aiff()!, // 80
    R.file.jingleLow3Aiff()!,
    R.file.jingleLow4Aiff()!,
    R.file.jingleLow5Aiff()!,
    R.file.jingleLow6Aiff()!,
    R.file.jingleLow7Aiff()!,
    R.file.jingleLowMed1Aiff()!, // 86
    R.file.jingleLowMed2Aiff()!,
    R.file.jingleMed1Aiff()!, // 88
    R.file.jingleMed10Aiff()!,
    R.file.jingleMed2Aiff()!,
    R.file.jingleMed3Aiff()!,
    R.file.jingleMed4Aiff()!,
    R.file.jingleMed5Aiff()!,
    R.file.jingleMed6Aiff()!,
    R.file.jingleMed8Aiff()!,
    R.file.jingleMed9Aiff()!,
    R.file.jingleMedHigh1Aiff()!, // 97
    R.file.jingleMedHigh2Aiff()!,
    R.file.jingleMedHigh3Aiff()!
]
