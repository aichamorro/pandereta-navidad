//
//  TambourineSounds.swift
//  pandereta-navidad
//
//  Created by Alberto Chamorro on 16/12/2017.
//  Copyright © 2017 Onset Bits. All rights reserved.
//

import Foundation

class TambourineInstrument: AudioKitInstrument {
    override func getSoundFilenames() -> [URL] {
        return TambourineSounds
    }

    override func filename(for event: InstrumentEvent, intensity: Float) -> String? {
        switch event.name {
        case TambourineInstrumentEvents.OuterRadius.rawValue:
            return nextOuterSoundForIntensity(intensity)

        case TambourineInstrumentEvents.InnerRadius.rawValue:
            return nextInnerSoundForIntensity(intensity)

        case TambourineInstrumentEvents.Center.rawValue:
            return nextCenterSoundForIntensity(intensity)

        case TambourineInstrumentEvents.Jingles.rawValue:
            return nextJingleSoundForIntensity(intensity)

        default:
            return R.file.palmaAiff()?.lastPathComponent
        }
    }
}
