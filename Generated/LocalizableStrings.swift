//
//  LocalizableStrings.swift
//
//  WARNING: This file has been autogenerated, manual changes in this file
//  will be lost. Modify the LocalizableStrings.json file instead.
//

import Foundation

enum LocalizableStrings {
  static private let __bundle = Bundle(for: DummyClass.self)

  enum Graph {
    enum Errors {
      internal static func noData(metric: String) -> String {
        let format = NSLocalizedString("graph.errors.noData", bundle: LocalizableStrings.__bundle, value: "There is no data recorded for %@", comment: "")

        return String.localizedStringWithFormat(format, metric)
      }
      internal static func ok() -> String {
        return NSLocalizedString("graph.errors.ok", bundle: LocalizableStrings.__bundle, value: "Ok", comment: "")
      }
    }
  }
  enum HistoricalData {
    enum RemoveWarning {
      internal static func no() -> String {
        return NSLocalizedString("historical-data.remove-warning.no", bundle: LocalizableStrings.__bundle, value: "Cancel", comment: "")
      }
      internal static func title() -> String {
        return NSLocalizedString("historical-data.remove-warning.title", bundle: LocalizableStrings.__bundle, value: "Warning", comment: "")
      }
      internal static func message() -> String {
        return NSLocalizedString("historical-data.remove-warning.message", bundle: LocalizableStrings.__bundle, value: "Removing this record will remove the other readings of this record. Do you want to delete it?", comment: "")
      }
      internal static func yes() -> String {
        return NSLocalizedString("historical-data.remove-warning.yes", bundle: LocalizableStrings.__bundle, value: "Yes", comment: "")
      }
    }
    enum Errors {
      enum Dialog {
        internal static func title() -> String {
          return NSLocalizedString("historical-data.errors.dialog.title", bundle: LocalizableStrings.__bundle, value: "Error", comment: "")
        }
        internal static func Ok() -> String {
          return NSLocalizedString("historical-data.errors.dialog.Ok", bundle: LocalizableStrings.__bundle, value: "Ok", comment: "")
        }
      }
      internal static func noData(metric: String) -> String {
        let format = NSLocalizedString("historical-data.errors.no-data", bundle: LocalizableStrings.__bundle, value: "There is no data recorded for %@", comment: "")

        return String.localizedStringWithFormat(format, metric)
      }
    }
  }
  enum Measures {
    enum BmiRating {
      internal static func severylyObese() -> String {
        return NSLocalizedString("measures.bmi-rating.severyly-obese", bundle: LocalizableStrings.__bundle, value: "Severely obese", comment: "")
      }
      internal static func overweight() -> String {
        return NSLocalizedString("measures.bmi-rating.overweight", bundle: LocalizableStrings.__bundle, value: "Overweight", comment: "")
      }
      internal static func healthyweight() -> String {
        return NSLocalizedString("measures.bmi-rating.healthyweight", bundle: LocalizableStrings.__bundle, value: "Healthy weight", comment: "")
      }
      internal static func obese() -> String {
        return NSLocalizedString("measures.bmi-rating.obese", bundle: LocalizableStrings.__bundle, value: "Obese", comment: "")
      }
      internal static func superObese() -> String {
        return NSLocalizedString("measures.bmi-rating.super-obese", bundle: LocalizableStrings.__bundle, value: "Super obese", comment: "")
      }
      internal static func morbidlyObese() -> String {
        return NSLocalizedString("measures.bmi-rating.morbidly-obese", bundle: LocalizableStrings.__bundle, value: "Morbidly obese", comment: "")
      }
      internal static func underweight() -> String {
        return NSLocalizedString("measures.bmi-rating.underweight", bundle: LocalizableStrings.__bundle, value: "Underweight", comment: "")
      }
    }
    enum BodyMetrics {
      internal static func height() -> String {
        return NSLocalizedString("measures.body-metrics.height", bundle: LocalizableStrings.__bundle, value: "Height", comment: "")
      }
      internal static func water() -> String {
        return NSLocalizedString("measures.body-metrics.water", bundle: LocalizableStrings.__bundle, value: "Water", comment: "")
      }
      internal static func bmi() -> String {
        return NSLocalizedString("measures.body-metrics.bmi", bundle: LocalizableStrings.__bundle, value: "BMI", comment: "")
      }
      internal static func bodyFat() -> String {
        return NSLocalizedString("measures.body-metrics.body-fat", bundle: LocalizableStrings.__bundle, value: "Body Fat", comment: "")
      }
      enum Units {
        internal static func percentage() -> String {
          return NSLocalizedString("measures.body-metrics.units.percentage", bundle: LocalizableStrings.__bundle, value: "%", comment: "")
        }
        internal static func height() -> String {
          return NSLocalizedString("measures.body-metrics.units.height", bundle: LocalizableStrings.__bundle, value: "cm", comment: "")
        }
        internal static func weight() -> String {
          return NSLocalizedString("measures.body-metrics.units.weight", bundle: LocalizableStrings.__bundle, value: "kg", comment: "")
        }
        internal static func bmi() -> String {
          return NSLocalizedString("measures.body-metrics.units.bmi", bundle: LocalizableStrings.__bundle, value: " ", comment: "")
        }
      }
      internal static func leanBodyWeight() -> String {
        return NSLocalizedString("measures.body-metrics.leanBody-weight", bundle: LocalizableStrings.__bundle, value: "Lean Body Weight", comment: "")
      }
      internal static func weight() -> String {
        return NSLocalizedString("measures.body-metrics.weight", bundle: LocalizableStrings.__bundle, value: "Weight", comment: "")
      }
      internal static func muscle() -> String {
        return NSLocalizedString("measures.body-metrics.muscle", bundle: LocalizableStrings.__bundle, value: "Muscle", comment: "")
      }
    }
  }
  enum Records {
    enum Latest {
      internal static func title() -> String {
        return NSLocalizedString("records.latest.title", bundle: LocalizableStrings.__bundle, value: "Last measurement", comment: "")
      }
    }
  }
  enum Insights {
    enum Periods {
      internal static func year() -> String {
        return NSLocalizedString("insights.periods.year", bundle: LocalizableStrings.__bundle, value: "Year", comment: "")
      }
      internal static func month() -> String {
        return NSLocalizedString("insights.periods.month", bundle: LocalizableStrings.__bundle, value: "Month", comment: "")
      }
      internal static func day() -> String {
        return NSLocalizedString("insights.periods.day", bundle: LocalizableStrings.__bundle, value: "Day", comment: "")
      }
      internal static func week() -> String {
        return NSLocalizedString("insights.periods.week", bundle: LocalizableStrings.__bundle, value: "Week", comment: "")
      }
    }
    internal static func title() -> String {
      return NSLocalizedString("insights.title", bundle: LocalizableStrings.__bundle, value: "Insights", comment: "")
    }
  }
}

private class DummyClass { }
